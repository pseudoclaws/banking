class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.references :account, foreign_key: true, index: true
      t.decimal :amount, precision: 8, scale: 2
      t.decimal :balance, precision: 8, scale: 2

      t.timestamps
    end
  end
end
