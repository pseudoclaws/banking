class CreateAccount
  class Form < Reform::Form
    model Account

    properties :user, :amount
    validates :amount, numericality: true

    collection :transactions, form: CreateTransaction::Form, populate_if_empty: Transaction
  end
end
