class CreateTransaction
  class Form < Reform::Form
    model Transaction
    property :account
    property :amount
    property :balance
    validates :account, presence: true
    validates :amount, :balance, numericality: true
  end
end
