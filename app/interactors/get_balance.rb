class GetBalance
  include Interactor

  before do
    current_user.errors.add(:base, 'Invalid account error') if context.account.blank?
    current_user.errors.add(:base, 'Authorization error') unless authorized?
    current_user.errors.add(:base, 'Invalid date error') if invalid_date?
    context.fail!(errors: current_user.errors) unless current_user.errors.blank?
  end

  def call
    pp context.date
    context.balance = if context.date == Date.today
                        context.account.amount
                      else
                        context.account.transactions
                          .select(:balance)
                          .where('date(created_at) = ?', context.date).order(id: :desc)
                          .first
                          &.balance
                      end
  end

  private

  def current_user
    @current_user ||= context.current_user || User.new
  end

  def authorized?
    current_user.errors.blank? && (current_user.admin? || context.account.user_id == current_user.id)
  end

  def invalid_date?
    context.date.blank? || context.date > Date.today
  end
end
