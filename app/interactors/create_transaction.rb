class CreateTransaction < CreationBase
  before do
    form.errors.add(:base, 'Authorization error') unless authorized?
    form.errors.add(:base, 'Not enough money') unless enough_money?
    form.errors.add(:base, 'Invalid account') if context.account.blank?
    context.fail!(errors: form.errors) unless form.errors.blank?
    context.balance = context.account.amount + context.amount
  end

  after do
    context.account.update(amount: form.balance)
    context.fail!(errors: context.account.errors) unless context.account.errors.blank?
  end

  protected

  def form
    @form ||= Form.new(Transaction.new)
  end

  private

  def authorized?
    context.current_user.admin?
  end

  def enough_money?
    context.amount > 0 || context.account.amount >= context.amount.abs
  end
end
