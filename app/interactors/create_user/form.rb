class CreateUser
  class Form < Reform::Form
    model User

    properties :email, :password, :password_confirmation, :authentication_token
    validates :email, :password, :password_confirmation, :authentication_token, presence: true

    collection :accounts, form: CreateAccount::Form, populate_if_empty: Account
  end
end
