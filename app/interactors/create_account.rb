class CreateAccount < CreationBase
  before do
    form.errors.add(:base, 'Authorization error') unless authorized?
    form.errors.add(:base, 'Invalid amount error') if context.amount.blank?
    form.errors.add(:base, 'Invalid user error') if context.user.blank?
    context.fail!(errors: form.errors) unless form.errors.blank?
  end

  protected

  def form
    @form ||= Form.new(Account.new)
  end

  private

  def authorized?
    context.current_user.admin?
  end
end
