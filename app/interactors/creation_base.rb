class CreationBase
  include Interactor

  def call
    context.fail!(errors: form.errors) unless form.validate(context.to_h)
    form.save
    context.form = form
  end

  protected

  def form
    raise NoMethodError, 'Method should be implemented in descendants'
  end
end
