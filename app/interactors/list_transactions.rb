class ListTransactions
  include Interactor

  before do
    current_user.errors.add(:base, 'Invalid account error') if context.account.blank?
    current_user.errors.add(:base, 'Authorization error') unless authorized?
    current_user.errors.add(:base, 'Invalid range') if invalid_range?
    context.fail!(errors: current_user.errors) unless current_user.errors.blank?
  end

  def call
    context.transactions = context
                             .account
                             .transactions
                             .where('date(created_at) >= ? and date(created_at) <= ?', context.start_date, context.end_date)
  end

  private

  def current_user
    @current_user ||= context.current_user || User.new
  end

  def authorized?
    current_user.errors.blank? && (current_user.admin? || context.account.user_id == current_user.id)
  end

  def invalid_range?
    context.start_date.blank? || context.end_date.blank? || context.start_date > context.end_date || context.end_date > Date.today
  end
end
