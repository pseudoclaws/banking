class CreateUser < CreationBase
  before do
    form.errors.add(:base, 'Authorization error') unless authorized?
    form.errors.add(:base, 'Invalid amount error') if context.amount.blank?
    context.fail!(errors: form.errors) unless form.errors.blank?
    context.authentication_token = Devise.friendly_token 30
    context.accounts = [{ amount: context.amount }]
  end

  protected

  def form
    @form ||= Form.new(User.new)
  end

  private

  def authorized?
    context.current_user.admin?
  end
end
