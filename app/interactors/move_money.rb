class MoveMoney
  include Interactor

  before do
    current_user.errors.add(:base, 'Authorization error') unless authorized?
    current_user.errors.add(:base, 'Invalid source account error') if context.src_account.blank?
    current_user.errors.add(:base, 'Invalid destination account error') if context.dst_account.blank?
    current_user.errors.add(:base, 'Invalid amount error') if context.amount.blank?
    context.fail!(errors: current_user.errors) unless current_user.errors.blank?
  end

  def call
    context.src_account.transaction do
      result = CreateTransaction.call(account: context.src_account, amount: -context.amount, current_user: current_user)
      context.fail!(errors: result.errors) && return if result.failure?
      result = CreateTransaction.call(account: context.dst_account, amount: context.amount, current_user: current_user)
      context.fail!(errors: result.errors) if result.failure?
    end
  end

  private

  def current_user
    @current_user ||= context.current_user || User.new
  end

  def authorized?
    current_user.admin?
  end
end
