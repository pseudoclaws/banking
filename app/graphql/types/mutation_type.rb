class Types::MutationType < Types::BaseObject
  field :create_user, mutation: Mutations::CreateUser
  field :create_account, mutation: Mutations::CreateAccount
  field :create_transaction, mutation: Mutations::CreateTransaction
  field :move_money, mutation: Mutations::MoveMoney
end
