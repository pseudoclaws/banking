module Types
  AccountType = GraphQL::ObjectType.define do
    name "Account"
    description "User's account"
    field :id, !types.ID
    field :user_id, !types.ID
    field :amount, !types.Float
    field :transactions do
      type types[Types::TransactionType]
      argument :size, types.Int, default_value: 0
      resolve ->(account, args, _) {
        account.transactions.limit(args[:size])
      }
    end
  end
end
