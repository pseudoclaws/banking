module Types
  UserType = GraphQL::ObjectType.define do
    name "User"
    description "Account's owner"
    field :id, !types.ID
    field :email, !types.String
    field :authentication_token, !types.String
    field :accounts do
      type types[Types::AccountType]
      argument :size, types.Int, default_value: 1
      resolve -> (user, args, _) {
        user.accounts.order(:id).limit(args[:size])
      }
    end
  end
end
