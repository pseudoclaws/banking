module Types
  TransactionType = GraphQL::ObjectType.define do
    name "Transaction"
    description "Account transaction"
    field :id, types.ID
    field :account_id, types.ID
    field :amount, types.Float
    field :balance, types.Float
  end
end
