module Types
  QueryType = GraphQL::ObjectType.define do
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    name "Query"
    description "The query root for this schema"

    field :transactions do
      type types[Types::TransactionType]
      argument :account_id, !types.ID
      argument :start_date, GraphQL::Types::ISO8601DateTime
      argument :end_date, GraphQL::Types::ISO8601DateTime

      resolve ->(_, args, ctx) {
        result = ListTransactions.call(
          account: Account.find(args[:account_id]),
          start_date: args[:start_date],
          end_date: args[:end_date],
          current_user: ctx[:current_user]
        )

        return if result.failure?
        result.transactions
      }
    end

    field :balance do
      type types.Float
      argument :account_id, !types.ID
      argument :date, GraphQL::Types::ISO8601DateTime
      resolve ->(_, args, ctx) {
        result = GetBalance.call(
          account: Account.find(args[:account_id]),
          date: args[:date],
          current_user: ctx[:current_user]
        )
        return if result.failure?
        result.balance
      }
    end

    field :authorize_user do
      type Types::UserType
      argument :email, !types.ID
      argument :password, !types.String
      resolve ->(_, args, _) {
        user = User.find_by(email: args[:email])
        return if user.blank? || !user.valid_password?(args[:password])
        user
      }
    end

    field :account do
      type Types::AccountType
      argument :id, !types.ID
      resolve -> (_, args, _) {
        Account.find(args[:id])
      }
    end

    field :transaction do
      type TransactionType
      argument :id, !types.ID
      resolve -> (_, args, _) {
        Transaction.find(args[:id])
      }
    end
  end
end
