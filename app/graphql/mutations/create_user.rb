module Mutations
  class CreateUser < GraphQL::Schema::Mutation
    argument :email, String, required: true
    argument :password, String, required: true
    argument :password_confirmation, String, required: true
    argument :amount, Float, required: false

    field :user, Types::UserType, null: true
    field :errors, [String], null: false

    def resolve(email:, password:, password_confirmation:, amount: 0)
      result = ::CreateUser.call(
        email:                 email,
        password:              password,
        password_confirmation: password_confirmation,
        amount:                amount,
        current_user:          context[:current_user]
      )

      if result.success?
        {
          user: result.form.model,
          errors: [],
        }
      else
        {
          comment: nil,
          errors: result.errors.full_messages
        }
      end
    end
  end
end
