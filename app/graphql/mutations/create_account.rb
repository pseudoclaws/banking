module Mutations
  class CreateAccount < GraphQL::Schema::Mutation
    argument :user_id, ID, required: true
    argument :amount, Float, required: true

    field :account, Types::AccountType, null: true
    field :errors, [String], null: false

    def resolve(user_id:, amount:)
      result = ::CreateAccount.call(user: User.find(user_id), amount: amount, current_user: context[:current_user])
      if result.success?
        {
          account: result.form.model,
          errors: [],
        }
      else
        {
          account: nil,
          errors: result.errors.full_messages
        }
      end
    end
  end
end
