module Mutations
  class MoveMoney < GraphQL::Schema::Mutation
    argument :src_account_id, ID, required: true
    argument :dst_account_id, ID, required: true
    argument :amount, Float, required: true

    field :errors, [String], null: false

    def resolve(src_account_id:, dst_account_id:, amount:)
      result = ::MoveMoney.call(
        src_account:  Account.find(src_account_id),
        dst_account:  Account.find(dst_account_id),
        amount:       amount,
        current_user: context[:current_user]
      )
      if result.success?
        {
          errors: []
        }
      else
        {
          errors: result.errors.full_messages
        }
      end
    end
  end
end
