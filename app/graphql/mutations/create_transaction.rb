module Mutations
  class CreateTransaction < GraphQL::Schema::Mutation
    argument :account_id, ID, required: true
    argument :amount, Float, required: true

    field :transaction, Types::TransactionType, null: true
    field :errors, [String], null: false

    def resolve(account_id:, amount:)
      result = ::CreateTransaction.call(account: Account.find(account_id), amount: amount, current_user: context[:current_user])
      if result.success?
        {
          transaction: result.form.model,
          errors: [],
        }
      else
        {
          transaction: nil,
          errors: result.errors.full_messages
        }
      end
    end
  end
end
