FactoryBot.define do
  factory :user do
    first_name { FFaker::Name.first_name }
    last_name  { FFaker::Name.last_name }
    email { FFaker::Internet.email }
    password { Devise.friendly_token }
    authentication_token { Devise.friendly_token 30 }
    factory :admin do
      admin true
    end
  end
end
