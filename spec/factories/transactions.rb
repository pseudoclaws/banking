FactoryBot.define do
  factory :transaction do
    account
    amount 100.0
    balance 100.0
  end
end
