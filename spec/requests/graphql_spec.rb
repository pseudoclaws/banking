require 'rails_helper'

RSpec.describe 'GraphQL requests', type: :request do
  let(:current_user) { FactoryBot.create(:admin) }
  let(:headers) do
    {
      'Content-Type' => 'application/json',
      'Accept' => '*/*',
      'X-User-Email' => current_user.email,
      'X-User-Token' => current_user.authentication_token
    }
  end
  describe 'createUser' do
    let(:email) { FFaker::Internet.email }
    let!(:password) { FFaker::Internet.password }
    let(:amount) { 100.0 }
    it 'creates new user' do
      query_string = "mutation createUser($email: String!, $password: String!, $password_confirmation: String!, $amount: Float!){createUser(email: $email, password: $password, passwordConfirmation: $password_confirmation, amount: $amount) {user {email authentication_token accounts { id amount }} errors}}"
      variables = {
        email: email,
        password: password,
        password_confirmation: password,
        amount: amount
      }

      post '/graphql', params: { query: query_string, variables: variables }.to_json, headers: headers
      data = JSON.parse(response.body)
      expect(data['data']).to be_present
      expect(data['data']['createUser']).to be_present
      expect(data['data']['createUser']['user']).to be_present
      expect(data['data']['createUser']['user']['email']).to be_present
      expect(data['data']['createUser']['user']['authentication_token']).to be_present
      expect(data['data']['createUser']['user']['accounts']).not_to be_blank
      expect(data['data']['createUser']['errors']).to be_blank
    end
  end

  describe 'createAccount' do
    let(:user) { FactoryBot.create(:user) }
    let(:amount) { 100.0 }
    it 'creates new account' do
      query_string = "mutation createAccount($user_id: ID!, $amount: Float!){createAccount(userId: $user_id, amount: $amount) {account {amount transactions { id }} errors}}"
      variables = {
        user_id: user.id,
        amount: amount
      }

      post '/graphql', params: { query: query_string, variables: variables }.to_json, headers: headers
      data = JSON.parse(response.body)
      expect(data['data']).to be_present
      expect(data['data']['createAccount']).to be_present
      expect(data['data']['createAccount']['account']).to be_present
      expect(data['data']['createAccount']['account']['amount']).to eq amount
      expect(data['data']['createAccount']['account']['transactions']).to eq []
      expect(data['data']['createAccount']['errors']).to be_blank
    end
  end

  describe 'createTransaction' do
    let(:account) { FactoryBot.create(:account) }
    let(:amount) { -100.0 }
    it 'creates new account' do
      query_string = "mutation createTransaction($account_id: ID!, $amount: Float!){createTransaction(accountId: $account_id, amount: $amount) {transaction {amount balance} errors}}"
      variables = {
        account_id: account.id,
        amount: amount
      }

      post '/graphql', params: { query: query_string, variables: variables }.to_json, headers: headers
      data = JSON.parse(response.body)
      expect(data['data']).to be_present
      expect(data['data']['createTransaction']).to be_present
      expect(data['data']['createTransaction']['transaction']).to be_present
      expect(data['data']['createTransaction']['transaction']['amount']).to eq amount
      expect(data['data']['createTransaction']['transaction']['balance']).to be_zero
      expect(data['data']['createTransaction']['errors']).to be_blank
    end
  end

  describe 'moveMoney' do
    let(:src_account) { FactoryBot.create(:account) }
    let(:dst_account) { FactoryBot.create(:account) }
    let(:amount) { 100.0 }
    it 'moves money' do
      query_string = "mutation moveMoney($src_account_id: ID!, $dst_account_id: ID!, $amount: Float!){moveMoney(srcAccountId: $src_account_id, dstAccountId: $dst_account_id, amount: $amount) {errors}}"
      variables = {
        src_account_id: src_account.id,
        dst_account_id: dst_account.id,
        amount: amount
      }

      post '/graphql', params: { query: query_string, variables: variables }.to_json, headers: headers
      data = JSON.parse(response.body)
      expect(data['data']).to be_present
      expect(data['data']['moveMoney']).to be_present
      expect(data['data']['moveMoney']['errors']).to be_blank
    end
  end

  describe 'listTransactions' do
    let!(:account) { FactoryBot.create(:account) }
    let!(:t1) { FactoryBot.create(:transaction, account: account, amount: 100.0) }
    let!(:t2) { FactoryBot.create(:transaction, account: account, amount: 100.0) }
    let(:start_date) { Date.today.iso8601 }
    let(:end_date) { Date.today.iso8601 }

    it 'lists transactions' do
      query_string = "query transactions($account_id: ID!, $start_date: ISO8601DateTime, $end_date: ISO8601DateTime){transactions(account_id: $account_id, start_date: $start_date, end_date: $end_date){id amount}}"
      variables = {
        account_id: account.id,
        start_date: start_date,
        end_date: end_date
      }

      post '/graphql', params: { query: query_string, variables: variables }.to_json, headers: headers
      data = JSON.parse(response.body)
      pp data
      expect(data['data']).to be_present
      expect(data['data']['transactions']).not_to be_blank
    end
  end

  describe 'getBalance' do
    let!(:account) { FactoryBot.create(:account) }
    let(:date) { Date.today.iso8601 }

    it 'lists transactions' do
      query_string = "query balance($account_id: ID!, $date: ISO8601DateTime){balance(account_id: $account_id, date: $date)}"
      variables = {
        account_id: account.id,
        date: date
      }

      post '/graphql', params: { query: query_string, variables: variables }.to_json, headers: headers
      data = JSON.parse(response.body)
      expect(data['data']).to be_present
      expect(data['data']['balance']).to eq 100.0
    end
  end
end
