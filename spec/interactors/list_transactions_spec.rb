require 'rails_helper'

RSpec.shared_context 'list transactions success' do
  it 'has success status' do
    expect(subject.success?).to eq true
  end
  it 'has transactions' do
    expect(subject.transactions).not_to be_empty
  end
end

RSpec.shared_context 'list transactions failure' do
  it 'has failure status' do
    expect(subject.failure?).to eq true
  end
  it 'has no transactions' do
    expect(subject.transactions).to be_blank
  end
  it 'has errors' do
    expect(subject.errors.full_messages).not_to be_empty
  end
end

RSpec.describe ListTransactions, type: :class do
  let(:params) do
    transactions_params.merge(current_user: current_user)
  end

  let(:subject) { ListTransactions.call(params) }

  context :with_valid_attributes do
    let!(:account) { FactoryBot.create(:account) }
    let!(:t1) { FactoryBot.create(:transaction, account: account, created_at: Date.yesterday) }
    let!(:t2) { FactoryBot.create(:transaction, account: account)}

    context :with_valid_admin do
      let(:current_user) { FactoryBot.create(:admin) }

      context 'for valid range' do
        let(:transactions_params) do
          {
            account: account,
            start_date: Date.yesterday,
            end_date: Date.today
          }
        end
        it_behaves_like 'list transactions success'
      end

      context 'for invalid range' do
        let(:transactions_params) do
          {
            account: account,
            start_date: Date.yesterday,
            end_date: Date.tomorrow
          }
        end
        it_behaves_like 'list transactions failure'
      end
    end

    context :with_authorized_user do
      let(:current_user) { account.user }

      context 'for valid range' do
        let(:transactions_params) do
          {
            account: account,
            start_date: Date.yesterday,
            end_date: Date.today
          }
        end
        it_behaves_like 'list transactions success'
      end

      context 'for invalid range' do
        let(:transactions_params) do
          {
            account: account,
            start_date: Date.yesterday,
            end_date: Date.tomorrow
          }
        end
        it_behaves_like 'list transactions failure'
      end
    end

    context :with_unauthorized_user do
      let(:current_user) { FactoryBot.create(:user) }

      context 'for valid range' do
        let(:transactions_params) do
          {
            account: account,
            start_date: Date.yesterday,
            end_date: Date.today
          }
        end
        it_behaves_like 'list transactions failure'
      end

      context 'for invalid range' do
        let(:transactions_params) do
          {
            account: account,
            start_date: Date.yesterday,
            end_date: Date.tomorrow
          }
        end
        it_behaves_like 'list transactions failure'
      end
    end
  end

  context :with_invalid_attributes do
    let(:current_user) { FactoryBot.create(:admin) }
    let(:transactions_params) do
      {
        account: nil
      }
    end
    it_behaves_like 'list transactions failure'
  end
end
