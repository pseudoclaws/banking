require 'rails_helper'

RSpec.shared_context 'get balance success' do
  it 'has success status' do
    expect(subject.success?).to eq true
  end
  it 'has valid balance' do
    expect(subject.balance).to eq account.amount
  end
end

RSpec.shared_context 'get balance failure' do
  it 'has failure status' do
    expect(subject.failure?).to eq true
  end
  it 'has invalid balance' do
    expect(subject.balance).to be_blank
  end
  it 'has errors' do
    expect(subject.errors.full_messages).not_to be_empty
  end
end

RSpec.describe GetBalance, type: :class do
  let(:params) do
    balance_params.merge(current_user: current_user)
  end

  let(:subject) { GetBalance.call(params) }

  context :with_valid_attributes do
    let!(:account) { FactoryBot.create(:account) }

    let(:balance_params) do
      {
        account: account
      }
    end


    context :with_valid_admin do
      let(:current_user) { FactoryBot.create(:admin) }

      context 'for today' do
        let(:balance_params) do
          {
            account: account,
            date: Date.today
          }
        end
        it_behaves_like 'get balance success'
      end

      context 'for yesterday' do
        let!(:t1) { FactoryBot.create(:transaction, account: account, amount: -100, balance: 100.0, created_at: Date.yesterday)}
        let(:balance_params) do
          {
            account: account,
            date: Date.yesterday
          }
        end
        it_behaves_like 'get balance success'
      end

      context 'for tomorrow' do
        let(:balance_params) do
          {
            account: account,
            date: Date.tomorrow
          }
        end
        it_behaves_like 'get balance failure'
      end
    end

    context :with_authorized_user do
      let(:current_user) { account.user }

      context 'for today' do
        let(:balance_params) do
          {
            account: account,
            date: Date.today
          }
        end
        it_behaves_like 'get balance success'
      end

      context 'for yesterday' do
        let!(:t1) { FactoryBot.create(:transaction, account: account, amount: -100, balance: 100.0, created_at: Date.yesterday)}
        let(:balance_params) do
          {
            account: account,
            date: Date.yesterday
          }
        end
        it_behaves_like 'get balance success'
      end

      context 'for tomorrow' do
        let(:balance_params) do
          {
            account: account,
            date: Date.tomorrow
          }
        end
        it_behaves_like 'get balance failure'
      end
    end

    context :with_unauthorized_user do
      let(:current_user) { FactoryBot.create(:user) }
      context 'for today' do
        let(:balance_params) do
          {
            account: account,
            date: Date.today
          }
        end
        it_behaves_like 'get balance failure'
      end

      context 'for yesterday' do
        let!(:t1) { FactoryBot.create(:transaction, account: account, amount: -100, balance: 0, created_at: Date.yesterday)}
        let(:balance_params) do
          {
            account: account,
            date: Date.yesterday
          }
        end
        it_behaves_like 'get balance failure'
      end

      context 'for tomorrow' do
        let(:balance_params) do
          {
            account: account,
            date: Date.tomorrow
          }
        end
        it_behaves_like 'get balance failure'
      end
    end
  end

  context :with_invalid_attributes do
    let(:current_user) { FactoryBot.create(:admin) }
    let(:balance_params) do
      {
        account: nil
      }
    end
    it_behaves_like 'get balance failure'
  end
end
