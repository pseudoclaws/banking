require 'rails_helper'

RSpec.shared_context "failed to create transaction" do
  it 'has failure status' do
    expect(subject.failure?).to eq true
  end

  it 'has error messages' do
    expect(subject.errors.full_messages).not_to be_empty
  end

  it 'does not create new transaction' do
    expect { subject }.not_to change { Transaction.count }
  end
end

RSpec.describe CreateTransaction, type: :class do
  let(:params) do
    account_params.merge(current_user: current_user)
  end

  let(:subject) { CreateTransaction.call(params) }

  context :with_valid_attributes do
    let!(:account) { FactoryBot.create(:account) }
    let(:account_params) do
      {
        account: account,
        amount: 100.0
      }
    end

    context :with_valid_admin do
      let(:current_user) { FactoryBot.create(:admin) }

      it 'has success status' do
        expect(subject.success?).to eq true
      end

      it 'creates new account for user' do
        expect { subject }.to change { Transaction.count }.by(1)
      end

      context :with_overdraft_amount do
        let(:account_params) do
          {
            account: account,
            amount: -1000.0
          }
        end
        it_behaves_like "failed to create transaction"
      end
    end

    context :with_unauthorized_user do
      let(:current_user) { FactoryBot.create(:user) }
      it_behaves_like "failed to create transaction"
    end
  end

  context :with_invalid_attributes do
    let(:current_user) { FactoryBot.create(:admin) }
    let(:account_params) do
      {
        amount: 100.0
      }
    end
    it_behaves_like "failed to create transaction"
  end
end
