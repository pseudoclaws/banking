require 'rails_helper'

RSpec.shared_context "failed to create account" do
  it 'has failure status' do
    expect(subject.failure?).to eq true
  end

  it 'has error messages' do
    expect(subject.errors.full_messages).not_to be_empty
  end

  it 'does not create new account for user' do
    expect { subject }.not_to change { Account.count }
  end
end

RSpec.describe CreateAccount, type: :class do
  let(:params) do
    account_params.merge(current_user: current_user)
  end

  let(:subject) { CreateAccount.call(params) }

  context :with_valid_attributes do
    let!(:user) { FactoryBot.create(:user) }
    let(:account_params) do
      {
        user: user,
        amount: 100.0
      }
    end

    context :with_valid_admin do
      let(:current_user) { FactoryBot.create(:admin) }

      it 'has success status' do
        expect(subject.success?).to eq true
      end

      it 'creates new account for user' do
        expect { subject }.to change { Account.count }.by(1)
      end
    end

    context :with_unauthorized_user do
      let(:current_user) { FactoryBot.create(:user) }
      it_behaves_like "failed to create account"
    end
  end

  context :with_invalid_attributes do
    let(:current_user) { FactoryBot.create(:admin) }
    let(:account_params) do
      {
        amount: 100.0
      }
    end
    it_behaves_like "failed to create account"
  end
end
