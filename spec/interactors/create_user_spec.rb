require 'rails_helper'

RSpec.shared_context "failed to create user" do
  it 'has failure status' do
    expect(subject.failure?).to eq true
  end

  it 'has error messages' do
    expect(subject.errors.full_messages).not_to be_empty
  end

  it 'does not create new user' do
    expect { subject }.not_to change { User.count }
  end

  it 'does not create new account for user' do
    expect { subject }.not_to change { Account.count }
  end
end

RSpec.describe CreateUser, type: :class do
  let(:params) do
    account_params.merge(current_user: current_user)
  end

  let(:subject) { CreateUser.call(params) }

  context :with_valid_attributes do
    let!(:password) { Devise.friendly_token }
    let(:account_params) do
      {
        email: FFaker::Internet.email,
        password: password,
        password_confirmation: password,
        amount: 100.0
      }
    end

    context :with_valid_admin do
      let!(:current_user) { FactoryBot.create(:admin) }

      it 'has success status' do
        result = subject
        expect(result.success?).to eq true
      end

      it 'creates new user' do
        expect { subject }.to change { User.count }.by(1)
      end

      it 'creates new account for user' do
        expect { subject }.to change { Account.count }.by(1)
      end

      it 'binds new account to user' do
        subject
        expect(User.find_by(email: account_params[:email]).accounts.count).to eq 1
      end
    end

    context :with_unauthorized_user do
      let!(:current_user) { FactoryBot.create(:user) }
      it_behaves_like "failed to create user"
    end
  end

  context :with_invalid_attributes do
    let!(:current_user) { FactoryBot.create(:admin) }
    let(:account_params) do
      {
        email: FFaker::Internet.email
      }
    end
    it_behaves_like "failed to create user"
  end
end
