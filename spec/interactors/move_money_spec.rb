require 'rails_helper'

RSpec.shared_context 'successful money move' do
  it 'has success status' do
    expect(subject.success?).to eq true
  end
  it 'has creates two transactions' do
    expect { subject }.to change { Transaction.count }.by(2)
  end
  it 'updates source account' do
    subject
    src_account.reload
    expect(src_account.amount).to eq 0
  end
  it 'updates destination account' do
    subject
    dst_account.reload
    expect(dst_account.amount).to eq 200
  end
end

RSpec.shared_context 'unsuccessful money move' do
  it 'has failed status' do
    expect(subject.failure?).to eq true
  end
  it 'has does not create transactions' do
    expect { subject }.not_to change { Transaction.count }
  end
  it 'does not update source account' do
    subject
    src_account.reload
    expect(src_account.amount).to eq 100
  end
  it 'does not update destination account' do
    subject
    dst_account.reload
    expect(dst_account.amount).to eq 100
  end
end

RSpec.describe MoveMoney, type: :class do
  let(:params) do
    move_params.merge(current_user: current_user)
  end

  let(:subject) { MoveMoney.call(params) }

  let!(:src_account) { FactoryBot.create(:account) }
  let!(:dst_account) { FactoryBot.create(:account) }

  context :with_valid_attributes do
    context :with_valid_amount do
      let(:move_params) do
        {
          src_account: src_account,
          dst_account: dst_account,
          amount: 100
        }
      end
      context :with_valid_admin do
        let(:current_user) { FactoryBot.create(:admin) }
        it_behaves_like 'successful money move'
      end
      context :with_authorized_user do
        let(:current_user) { src_account.user }
        it_behaves_like 'unsuccessful money move'
      end
      context :with_unauthorized_user do
        let(:current_user) { FactoryBot.create(:user) }
        it_behaves_like 'unsuccessful money move'
      end
    end
  end

  context :with_invalid_attributes do
    let(:current_user) { FactoryBot.create(:admin) }
    let(:move_params) do
      {
        src_account: src_account,
        dst_account: dst_account,
        amount: nil
      }
    end
    it_behaves_like 'unsuccessful money move'
  end
end
